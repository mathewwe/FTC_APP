package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

/**
 * Created by mathe_000 on 8/27/2017.
 */
@Autonomous(name = "REV IMU Test")
@Disabled
public class GyroTest extends OpMode{
    AdafruitIMU imu = new AdafruitIMU();

    @Override
    public void init(){
        imu.init(hardwareMap, "imu");
    }
    @Override
    public void start(){
        imu.start();
    }
    @Override
    public void loop(){
        imu.loop();
        telemetry.addData("Heading: ", imu.heading);
    }
}
