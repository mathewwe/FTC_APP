package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.motors.NeveRest40Gearmotor;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.configuration.MotorConfigurationType;

/**
 * Created by mathe_000 on 8/28/2017.
 */
@TeleOp(name = "REV Motor Test")
public class ChassisTest extends OpMode {
    AdafruitIMU imu = new AdafruitIMU();
    ComputerVision CV = new ComputerVision();
    DcMotor l;
    DcMotor r;
    ColorSensor rev;
    DigitalChannel touch;

    @Override
    public void init(){
        imu.init(hardwareMap, "imu");
        l = hardwareMap.dcMotor.get("l");
        r = hardwareMap.dcMotor.get("r");
        rev = hardwareMap.colorSensor.get("rev"); // TODO: 9/21/2017 Try mapping in REV Color Sensor example
        touch = hardwareMap.digitalChannel.get("touch");
        touch.setMode(DigitalChannel.Mode.INPUT);
        l.setDirection(DcMotorSimple.Direction.REVERSE);

        l.setMotorType(MotorConfigurationType.getMotorType(NeveRest40Gearmotor.class));
        r.setMotorType(MotorConfigurationType.getMotorType(NeveRest40Gearmotor.class));

        l.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        r.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        CV.init();
    }
    @Override
    public void start(){
        imu.start();
        l.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        r.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rev.enableLed(true);
        CV.start();
    }
    @Override
    public void loop(){
        imu.loop();
        l.setTargetPosition(1120);
        r.setTargetPosition(1120);
        l.setPower(1);
        r.setPower(1);
        telemetry.addData("Heading: ", imu.heading);
        telemetry.addData("R: ", rev.red());
        telemetry.addData("G: ", rev.green());
        telemetry.addData("B: ", rev.blue());
        telemetry.addData("ARGB: ", rev.argb());
        telemetry.addData("Touch Sensor: ", touch.getState());
    }
}
