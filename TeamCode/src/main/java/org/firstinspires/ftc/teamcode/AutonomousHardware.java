package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.exception.RobotCoreException;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by mathe on 12/11/2016.
 */

public class AutonomousHardware {

    public DcMotor motorFrontRight;
    public DcMotor motorFrontLeft;
    public DcMotor motorRearLeft;
    public DcMotor motorRearRight;
    public DcMotor launcher;
    public DcMotor collector;
    public AdafruitIMUold boschBNO055; // TODO: 8/27/2017 Replace this with new integrated IMU class
    public ModernRoboticsI2cRangeSensor Ultrasonic;
    public Servo ball;
    public ColorSensor beacon;
    public OpticalDistanceSensor ODS;
    public long systemTime;//Relevant values of System.nanoTime
    HardwareMap HWMAP  = null;
    public AutonomousHardware(){

    }
    public void init(HardwareMap hwMap){
        HWMAP = hwMap;
        motorFrontLeft = HWMAP.dcMotor.get("FrontLeft");
        motorFrontRight = HWMAP.dcMotor.get("FrontRight");
        motorRearLeft = HWMAP.dcMotor.get("RearLeft");
        motorRearRight = HWMAP.dcMotor.get("RearRight");
        launcher = HWMAP.dcMotor.get("launcher");
        collector = HWMAP.dcMotor.get("collector");
        ball = HWMAP.servo.get("ball");
        Ultrasonic = HWMAP.get(ModernRoboticsI2cRangeSensor.class, "Ultrasonic");
        ODS = HWMAP.opticalDistanceSensor.get("ODS");
        beacon = HWMAP.colorSensor.get("beacon");
        systemTime = System.nanoTime();
        try {
            boschBNO055 = new AdafruitIMUold(HWMAP, "bno055", (byte)(AdafruitIMUold.BNO055_ADDRESS_A), (byte) AdafruitIMUold.OPERATION_MODE_IMU);
        } catch (RobotCoreException e){
            Log.i("FtcRobotController", "Exception: " + e.getMessage());
        }
        Log.i("FtcRobotController", "IMU Init method finished in: "
                + (-(systemTime - (systemTime = System.nanoTime()))) + " ns.");
        beacon.enableLed(false);

        motorFrontLeft.setDirection(DcMotor.Direction.REVERSE);
        motorRearLeft.setDirection(DcMotor.Direction.FORWARD);
        motorFrontRight.setDirection(DcMotor.Direction.FORWARD);
        motorRearRight.setDirection(DcMotor.Direction.REVERSE);

        motorFrontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorFrontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRearLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        motorRearRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        launcher.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }
    volatile double[] rollAngle = new double[2], pitchAngle = new double[2], yawAngle = new double[2];

}

