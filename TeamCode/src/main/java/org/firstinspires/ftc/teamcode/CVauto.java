package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;

/**
 * Created by mathe_000 on 2/12/2017.
 */
@Autonomous(name = "Computer Vision Autonomous")
@Disabled
//Left as example
public class CVauto extends AutonomousBaseCV {
    double power;
    @Override
    public void loop(){
        robot.boschBNO055.getIMUGyroAngles(robot.rollAngle, robot.pitchAngle, robot.yawAngle);
        switch (step){
            case 1://The equivalent of this code is expressed in the uncommented method below.


                /*if (((VuforiaTrackableDefaultListener) CV.Gears.getListener()).isVisible()) {
                    power = (CV.targetDistance(CV.Gears) - 200) * .005;
                    power = Range.clip(power, -.5, .5);
                    robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    robot.motorRearRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

                    robot.motorFrontLeft.setPower(-power);
                    robot.motorFrontRight.setPower(power);
                    robot.motorRearLeft.setPower(power);
                    robot.motorRearRight.setPower(-power);
                    telemetry.addData(CV.Gears.getName() + "-visible", "Yes");
                } else {
                    telemetry.addData(CV.Gears.getName() + "-visible", "No");  // isn't called after target is found
                }
                if(Math.abs(CV.targetDistance(CV.Gears) - 200) < 4 && elapsedTime() >= .5){
                    step++;
                }
                else if(Math.abs(CV.targetDistance(CV.Gears) - 200) < 4);
                else{
                    start = System.currentTimeMillis();
                }*/
                strafeToCVPosition(300, .5, .005, CV.Gears);
                break;
            case 2:
                turnRightP(0, 1., .01, 1);
            case 3: //The equivalent of this code is expressed in the uncommented method below.


                /*if (((VuforiaTrackableDefaultListener) CV.Gears.getListener()).isVisible()) {
                    if(CV.rawPose(CV.Gears) != null){
                        power = (CV.getPose(CV.Gears, 1) - 0) * .005;
                        power = Range.clip(power, -.15, .15);
                    }
                    robot.motorFrontLeft.setPower(-power);
                    robot.motorFrontRight.setPower(-power);
                    robot.motorRearLeft.setPower(-power);
                    robot.motorRearRight.setPower(-power);
                    telemetry.addData(CV.Gears.getName() + "-visible", "Yes");
                } else {
                    telemetry.addData(CV.Gears.getName() + "-visible", "No");  // isn't called after target is found
                }
                if(CV.rawPose(CV.Gears) != null) {
                    if(Math.abs(CV.getPose(CV.Gears, 1)) < 4 && elapsedTime() >= .5){
                        step++;
                    }
                    else if(Math.abs(CV.getPose(CV.Gears, 1)) < 4);
                    else{
                        start = System.currentTimeMillis();
                    }
                }*/
                driveStraightToCVPosition(15, .15, .005, CV.Gears);
                break;
            default:
                stopMotors();
                break;
        }
        telemetry.addData(CV.Gears.getName() + "-Distance", CV.targetDistance(CV.Gears));
        if(CV.rawPose(CV.Gears) != null){
            telemetry.addData("Gears Pose", CV.getPose(CV.Gears, 1));
        }
    }
}
