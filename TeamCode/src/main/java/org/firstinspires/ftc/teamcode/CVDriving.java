package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;

/**
 * Created by mathe_000 on 4/21/2017.
 */
@Autonomous(name = "CV Driving Test")
@Disabled
//WILL NOT WORK, LEFT AS EXAMPLE
public class CVDriving extends AutonomousBaseCV{
    @Override
    public void loop(){
        switch (step){
            case 1:
                robot.motorFrontLeft.setPower(.1);
                robot.motorFrontRight.setPower(-.1);
                waitInMilliseconds(2000);
                step++;
                break;
            case 2:
                robot.motorFrontLeft.setPower(0);
                robot.motorFrontRight.setPower(0);
                waitInMilliseconds(1000);
                step++;
            case 3:
                if (CV.rawPose(CV.Gears) != null){
                    step++;
                }
                else{
                    step = 1;
                }
                break;
            case 4:
                robot.motorFrontLeft.setPower(driveStraightCVPower(200, .15, .001, CV.Gears) - turnCVPower(0, .06, .001, CV.Gears));
                robot.motorFrontRight.setPower(driveStraightCVPower(200, .15, .001, CV.Gears) + turnCVPower(0, .06, .001, CV.Gears));
                if(CV.rawPose(CV.Gears) != null) {
                    if(Math.abs(CV.targetDistance(CV.Gears) - 200) < 4 && elapsedTime() >= 2){
                        step++;
                    }
                    else if(Math.abs(CV.targetDistance(CV.Gears) - 200) < 4);
                    else{
                        start = System.currentTimeMillis();
                    }
                }
                break;
            default:
                break;
        }
        AutoTelemetry();
    }
}
