package org.firstinspires.ftc.teamcode;

import android.util.Log;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cRangeSensor;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.exception.RobotCoreException;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import java.util.Timer;
import java.util.concurrent.Future;

public abstract class AutonomousBaseNoCV extends OpMode { // TODO: 8/27/2017 Replace IMU functions with new functions using integrated IMU
    AutonomousHardware robot = new AutonomousHardware();
    double start;
    public double elapsedTime(){
        long now = System.currentTimeMillis();
        return (now-start)/1000;
    }
    static double WHEEL_DIAMETER = 3.93701; //Wheel diameter in inches
    static double ENCODER_CPR = 1120; //Encoder Counts Per Revolution
    int launchTicks = 1680;

    /*public void goToUSReading(double cm, double power){ //Go to "cm" Centimeters away from the right wall.
        if(cm < robot.Ultrasonic.getDistance(DistanceUnit.CM)){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontLeft.setPower(power);
            robot.motorFrontRight.setPower(-power);
            robot.motorRearLeft.setPower(-power);
            robot.motorRearRight.setPower(power);
            if(Math.abs(robot.Ultrasonic.getDistance(DistanceUnit.CM)) < 1){
                step++;
        start = System.currentTimeMillis();

            }
        }
        if(cm > robot.Ultrasonic.getDistance(DistanceUnit.CM)){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontLeft.setPower(-power);
            robot.motorFrontRight.setPower(power);
            robot.motorRearLeft.setPower(power);
            robot.motorRearRight.setPower(-power);
            if(Math.abs(robot.Ultrasonic.getDistance(DistanceUnit.CM)) < 1){
                step++;
        start = System.currentTimeMillis();

            }
        }
    }*/
    public double adjustAngle(double angle) {
        if (angle > 180) angle -= 360;
        if (angle <= -180) angle += 360;
        return angle;
    }
    double errorL;
    double powerL;
    double errorR;
    double powerR;
    public void turnLeftP(double degrees, double speed, double kp, double timeout) {
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        errorL = adjustAngle(degrees- - robot.yawAngle[1]);// TODO: 8/27/2017 Check if this double negative was intentional
        if (elapsedTime() < timeout) {
            powerL = kp * errorL;
            powerL = Range.clip(powerL, -speed, +speed);
            robot.motorFrontLeft.setPower(powerL);
            robot.motorRearLeft.setPower(powerL);
            robot.motorFrontRight.setPower(-powerL);
            robot.motorRearRight.setPower(-powerL);
        }
        else{
            robot.motorFrontLeft.setPower(0);
            robot.motorRearLeft.setPower(0);
            robot.motorFrontRight.setPower(0);
            robot.motorRearRight.setPower(0);
            start = System.currentTimeMillis();
            step++;
        }
    }

    public void turnRightP(double degrees, double speed, double kp, double timeout) {
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        errorR = adjustAngle(degrees - robot.yawAngle[1]);
        if (elapsedTime() < timeout) {
            powerR = -(kp * errorR);
            powerR = Range.clip(powerR, -speed, +speed);
            robot.motorFrontLeft.setPower(-powerR);
            robot.motorRearLeft.setPower(-powerR);
            robot.motorFrontRight.setPower(powerR);
            robot.motorRearRight.setPower(powerR);
        }
        else{
            robot.motorFrontLeft.setPower(0);
            robot.motorRearLeft.setPower(0);
            robot.motorFrontRight.setPower(0);
            robot.motorRearRight.setPower(0);
            start = System.currentTimeMillis();
            step++;
        }
    }

    public void resetGyro(){ //Tested, found to be functional.
        robot.boschBNO055.startIMU();
        start = System.currentTimeMillis();
        step++;
    }
    public void resetGyroNoStep(){
        robot.boschBNO055.startIMU();
    }

    public void resetEncoders(){ //Resets motor encoders to 0 for next movement. TODO: Test if functional when put in the case advancement conditions of statements using encoders.
        robot.motorFrontLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        if(robot.motorFrontLeft.getCurrentPosition() == 0){
            start = System.currentTimeMillis();
            step++;
        }
    }
    enum DIR{FORWARD, REVERSE}

    //The following methods drive until a boolean condition entered as a parameter is reached.

    public void driveStraightUntil(DIR dir, double power, boolean condition){
        if(dir == DIR.FORWARD){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontLeft.setPower(-power);
            robot.motorFrontRight.setPower(-power);
            robot.motorRearLeft.setPower(-power);
            robot.motorRearRight.setPower(-power);

            if(condition){
                start = System.currentTimeMillis();
                step++;
            }
        }
        if(dir == DIR.REVERSE){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontLeft.setPower(power);
            robot.motorFrontRight.setPower(power);
            robot.motorRearLeft.setPower(power);
            robot.motorRearRight.setPower(power);

            if(condition){
                start = System.currentTimeMillis();
                step++;
            }
        }
    }
    public void driveDiagonalLeftUntil(DIR dir, double power, boolean condition){
        if(dir == DIR.FORWARD){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontRight.setPower(-power);
            robot.motorRearLeft.setPower(-power);
            robot.motorFrontLeft.setPower(0);
            robot.motorRearRight.setPower(0);

            if(condition){
                start = System.currentTimeMillis();
                step++;
            }
        }
        if(dir == DIR.REVERSE){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontRight.setPower(power);
            robot.motorRearLeft.setPower(power);
            robot.motorFrontLeft.setPower(0);
            robot.motorRearRight.setPower(0);

            if(condition){
                start = System.currentTimeMillis();
                step++;
            }
        }
    }
    public void driveDiagonalRightUntil(DIR dir, double power, boolean condition){
        if(dir == DIR.FORWARD){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontLeft.setPower(-power);
            robot.motorRearRight.setPower(-power);
            robot.motorFrontRight.setPower(0);
            robot.motorRearLeft.setPower(0);

            if(condition){
                start = System.currentTimeMillis();
                step++;
            }
        }
        if(dir == DIR.REVERSE){
            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

            robot.motorFrontLeft.setPower(power);
            robot.motorRearRight.setPower(power);
            robot.motorFrontRight.setPower(0);
            robot.motorRearLeft.setPower(0);

            if(condition){
                start = System.currentTimeMillis();
                step++;
            }
        }
    }
    public void driveLeftUntil(double power, boolean condition){
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        robot.motorFrontLeft.setPower(power);
        robot.motorFrontRight.setPower(-power);
        robot.motorRearLeft.setPower(-power);
        robot.motorRearRight.setPower(power);

        if(condition){
            start = System.currentTimeMillis();
            step++;
        }
    }
    public void driveRightUntil(double power, boolean condition){
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        robot.motorFrontLeft.setPower(-power);
        robot.motorFrontRight.setPower(power);
        robot.motorRearLeft.setPower(power);
        robot.motorRearRight.setPower(-power);

        if(condition){
            start = System.currentTimeMillis();
            step++;
        }
    }
    public void turnLeftUntil(double power, boolean condition){
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        robot.motorFrontLeft.setPower(power);
        robot.motorFrontRight.setPower(-power);
        robot.motorRearLeft.setPower(power);
        robot.motorRearRight.setPower(-power);

        if(condition){
            start = System.currentTimeMillis();
            step++;
        }
    }
    public void turnRightUntil(double power, boolean condition){
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        robot.motorFrontLeft.setPower(-power);
        robot.motorFrontRight.setPower(power);
        robot.motorRearLeft.setPower(-power);
        robot.motorRearRight.setPower(power);

        if(condition){
            start = System.currentTimeMillis();
            step++;
        }
    }

    //These methods drive until the specified distance or degree threshold has been met or exceeded.

    public void driveStraight(double driveDistance, double power){ //Drives straight for a specified amount of inches.
        if(driveDistance > 0){
            double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance;
            int roundedDistance = (int)Math.round(Distance);
            robot.motorFrontLeft.setTargetPosition(-roundedDistance);
            robot.motorFrontRight.setTargetPosition(-roundedDistance);
            robot.motorRearLeft.setTargetPosition(-roundedDistance);
            robot.motorRearRight.setTargetPosition(-roundedDistance);

            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            robot.motorFrontLeft.setPower(-power);
            robot.motorFrontRight.setPower(-power);
            robot.motorRearLeft.setPower(-power);
            robot.motorRearRight.setPower(-power);

            if(Math.abs(robot.motorFrontRight.getCurrentPosition()) >= Math.abs(robot.motorFrontRight.getTargetPosition())){
                start = System.currentTimeMillis();
                step++;
            }
        }
        if(driveDistance < 0){
            double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance;
            int roundedDistance = -(int)Math.round(Distance);
            robot.motorFrontLeft.setTargetPosition(roundedDistance);
            robot.motorFrontRight.setTargetPosition(roundedDistance);
            robot.motorRearLeft.setTargetPosition(roundedDistance);
            robot.motorRearRight.setTargetPosition(roundedDistance);

            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            robot.motorFrontLeft.setPower(power);
            robot.motorFrontRight.setPower(power);
            robot.motorRearLeft.setPower(power);
            robot.motorRearRight.setPower(power);

            if(Math.abs(robot.motorFrontRight.getCurrentPosition()) >= Math.abs(robot.motorFrontRight.getTargetPosition())){
                start = System.currentTimeMillis();
                step++;
            }
        }
    }
    public void driveDiagonalLeft(double driveDistance, double power){ //Using mecanum wheels, drives diagonal left forwards, diagonal right backwards for a specified amount of inches.
        if(driveDistance > 0){
            double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance * 2;
            int roundedDistance = (int)Math.round(Distance);
            robot.motorFrontRight.setTargetPosition(-roundedDistance);
            robot.motorRearLeft.setTargetPosition(-roundedDistance);

            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            robot.motorFrontRight.setPower(-power);
            robot.motorRearLeft.setPower(-power);
            robot.motorFrontLeft.setPower(0);
            robot.motorRearRight.setPower(0);

            if(Math.abs(robot.motorFrontRight.getCurrentPosition()) >= Math.abs(robot.motorFrontRight.getTargetPosition())){
                start = System.currentTimeMillis();
                step++;
            }
        }
        if(driveDistance < 0){
            double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance * 2;
            int roundedDistance = (int)Math.round(Distance);
            robot.motorFrontRight.setTargetPosition(roundedDistance);
            robot.motorRearLeft.setTargetPosition(roundedDistance);

            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            robot.motorFrontRight.setPower(power);
            robot.motorRearLeft.setPower(power);
            robot.motorFrontLeft.setPower(0);
            robot.motorRearRight.setPower(0);

            if(Math.abs(robot.motorFrontRight.getCurrentPosition()) >= Math.abs(robot.motorFrontRight.getTargetPosition())){
                start = System.currentTimeMillis();
                step++;
            }
        }
    }
    public void driveDiagonalRight(double driveDistance, double power){ //Using mecanum wheels, drives diagonal right forwards, diagonal right backwards for a specified amount of inches.
        if(driveDistance > 0){
            double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance * 2;
            int roundedDistance = (int)Math.round(Distance);
            robot.motorFrontLeft.setTargetPosition(-roundedDistance);
            robot.motorRearRight.setTargetPosition(-roundedDistance);

            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            robot.motorFrontLeft.setPower(-power);
            robot.motorRearRight.setPower(-power);
            robot.motorFrontRight.setPower(0);
            robot.motorRearLeft.setPower(0);

            if(Math.abs(robot.motorFrontLeft.getCurrentPosition()) >= Math.abs(robot.motorFrontLeft.getTargetPosition())){
                start = System.currentTimeMillis();
                step++;
            }
        }
        if(driveDistance < 0){
            double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance * 2;
            int roundedDistance = (int)Math.round(Distance);
            robot.motorFrontLeft.setTargetPosition(roundedDistance);
            robot.motorRearRight.setTargetPosition(roundedDistance);

            robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

            robot.motorFrontLeft.setPower(power);
            robot.motorRearRight.setPower(power);
            robot.motorFrontRight.setPower(0);
            robot.motorRearLeft.setPower(0);

            if(Math.abs(robot.motorFrontLeft.getCurrentPosition()) >= Math.abs(robot.motorFrontLeft.getTargetPosition())){
                start = System.currentTimeMillis();
                step++;
            }
        }
    }
    public void driveLeft(double driveDistance, double power){ //Using mecanum wheels, drives left for a specified amount of inches.
        double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance * 2;
        int roundedDistance = (int)Math.round(Distance);
        robot.motorFrontLeft.setTargetPosition(roundedDistance);
        robot.motorFrontRight.setTargetPosition(-roundedDistance);
        robot.motorRearLeft.setTargetPosition(-roundedDistance);
        robot.motorRearRight.setTargetPosition(roundedDistance);

        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        robot.motorFrontLeft.setPower(power);
        robot.motorFrontRight.setPower(-power);
        robot.motorRearLeft.setPower(-power);
        robot.motorRearRight.setPower(power);

        if(Math.abs(robot.motorFrontRight.getCurrentPosition()) >= Math.abs(robot.motorFrontRight.getTargetPosition())){
            start = System.currentTimeMillis();
            start = System.currentTimeMillis();
            step++;
        }
    }
    public void driveRight(double driveDistance, double power){ //Using mecanum wheels, drives right for a specified amount of inches.
        double Distance = (ENCODER_CPR/(WHEEL_DIAMETER * Math.PI)) * driveDistance * 2;
        int roundedDistance = (int)Math.round(Distance);
        robot.motorFrontLeft.setTargetPosition(-roundedDistance);
        robot.motorFrontRight.setTargetPosition(roundedDistance);
        robot.motorRearLeft.setTargetPosition(roundedDistance);
        robot.motorRearRight.setTargetPosition(-roundedDistance);

        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        robot.motorFrontLeft.setPower(-power);
        robot.motorFrontRight.setPower(power);
        robot.motorRearLeft.setPower(power);
        robot.motorRearRight.setPower(-power);

        if(Math.abs(robot.motorFrontLeft.getCurrentPosition()) >= Math.abs(robot.motorFrontLeft.getTargetPosition())){
            start = System.currentTimeMillis();
            step++;
        }
    }
    public void waitInMilliseconds(long time){ //Waiting method in a normal OpMode. Like sleep() in a linear OpMode.
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void stopMotors(){
        robot.motorFrontLeft.setPower(0);
        robot.motorFrontRight.setPower(0);
        robot.motorRearLeft.setPower(0);
        robot.motorRearRight.setPower(0);

        robot.motorFrontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robot.motorFrontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robot.motorRearLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robot.motorRearRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        if(robot.motorFrontLeft.getPower() == 0){
            start = System.currentTimeMillis();
            step++;
        }
    }
    public void AutoTelemetry(){
        // TELEMETRY
        telemetry.addData(">", "Front Left TGTPOS: " + robot.motorFrontLeft.getTargetPosition());
        telemetry.addData(">", "Front Right TGTPOS: " + robot.motorFrontRight.getTargetPosition());
        telemetry.addData(">", "Rear Left TGTPOS: " + robot.motorRearLeft.getTargetPosition());
        telemetry.addData(">", "Rear Right TGTPOS: " + robot.motorRearRight.getTargetPosition());
        telemetry.addData(">", "Front Left Pos" + robot.motorFrontLeft.getCurrentPosition());
        telemetry.addData(">", "Front Right Pos" + robot.motorFrontRight.getCurrentPosition());
        telemetry.addData(">", "Rear Left Pos" + robot.motorRearLeft.getCurrentPosition());
        telemetry.addData(">", "Rear Right Pos" + robot.motorRearRight.getCurrentPosition());
        telemetry.addData(">", "Case: " + step);
        telemetry.addData(">", "Headings(yaw): " + robot.yawAngle[1]);
    }
    long systemTime;//Relevant values of System.nanoTime
    int step;
    @Override
    public void init(){
        robot.init(hardwareMap);
    }
    @Override
    public void init_loop(){
        telemetry.addData(">", "GYROSCOPE CALIBRATED. READY TO START");
    }
    @Override
    public void start(){
        systemTime = System.nanoTime();
        start = System.currentTimeMillis();
        robot.boschBNO055.startIMU();
        Log.i("FtcRobotController", "IMU Start method finished in: "
                + (-(systemTime - (systemTime = System.nanoTime()))) + " ns.");
        robot.motorFrontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorFrontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorRearLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.motorRearRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.launcher.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        step = 1;
    }
}
