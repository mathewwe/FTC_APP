package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;

/**
 * Created by mathe_000 on 12/24/2016.
 */
@Autonomous(name = "CV Test")
@Disabled
public class CVtest extends OpMode {
    ComputerVision CV = new ComputerVision();
    @Override
    public void init(){
        CV.init();
    }
    @Override
    public void start(){
        CV.start();
    }
    @Override
    public void loop(){
        telemetry.addData("Pose (Gears)", CV.getPose(CV.Gears, 1));
        telemetry.addData("RawPose: ", CV.rawPose(CV.Gears));
        telemetry.addData("Distance (Gears)", CV.targetDistance(CV.Gears));
        telemetry.addData("Angle (Gears)", CV.targetAngle(CV.Gears));
    }
}
